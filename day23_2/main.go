package main

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"time"
	"unsafe"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	result, err := getCupLabels("315679824", 1000000, 10000000)
	if err != nil {
		return err
	}

	fmt.Println()
	fmt.Println(result)
	return nil
}

func getCupLabels(start string, size, steps int) (int, error) {
	cups, err := NewCircle(start, size)
	if err != nil {
		return 0, err
	}

	fmt.Println(unsafe.Sizeof(cups))

	done := make(chan bool)
	ticker := time.NewTicker(1 * time.Second)

	go func() {
		p := message.NewPrinter(language.English)
		for {
			select {
			case <-done:
				ticker.Stop()
				return
			case <-ticker.C:
				p.Printf("\rSteps: %d", cups.steps)
			}
		}
	}()

	for i := 0; i < steps; i++ {
		cups.Step()
	}
	oneNode := cups.Find(1)
	close(done)
	return oneNode.next.value * oneNode.next.next.value, nil
}

type Circle struct {
	start   *Node
	current *Node
	min     int
	max     int
	steps   int
	cache   map[int]*Node
}

func NewCircle(start string, size int) (*Circle, error) {
	cache := map[int]*Node{}
	min := math.MaxInt64
	max := 0
	startNode := &Node{}
	prev := startNode
	for _, num := range start {
		value, err := strconv.Atoi(string(num))
		if err != nil {
			return nil, err
		}

		if value < min {
			min = value
		}

		if value > max {
			max = value
		}

		newNode := &Node{
			value: value,
		}

		prev.next = newNode
		cache[value] = newNode

		prev = prev.next
	}

	for i := max + 1; i <= size; i++ {
		if i < min {
			min = i
		}
		if i > max {
			max = i
		}

		newNode := &Node{
			value: i,
		}

		prev.next = newNode
		cache[i] = newNode

		prev = prev.next
	}
	prev.next = startNode.next

	return &Circle{
		start:   startNode.next,
		current: startNode.next,
		min:     min,
		max:     max,
		cache:   cache,
	}, nil
}

func (c *Circle) Step() {
	popStart := c.current.next
	popEnd := popStart
	for i := 0; i < 3-1; i++ {
		popEnd = popEnd.next
	}
	// pop the sub-list
	c.current.next = popEnd.next
	popEnd.next = nil

	target := c.getDestinationValue(c.current.value, popStart)

	searchNode := c.Find(target)

	next := searchNode.next
	searchNode.next = popStart
	popEnd.next = next

	c.current = c.current.next
	c.steps++
}

func (c *Circle) Find(target int) *Node {
	return c.cache[target]
}

func (c *Circle) getDestinationValue(source int, popped *Node) int {
	target := source - 1
	if target < c.Min() {
		target = c.Max()
	}

	var poppedValues []int

	for i := 0; i < 3; i++ {
		poppedValues = append(poppedValues, popped.value)
		popped = popped.next
	}

outer:
	for {
		for _, value := range poppedValues {
			if value == target {
				target--
				if target < c.Min() {
					target = c.Max()
				}

				continue outer
			}
		}

		break
	}

	return target
}

func (c *Circle) Max() int {
	return c.max
}

func (c *Circle) Min() int {
	return c.min
}

type Node struct {
	value int
	next  *Node
}
