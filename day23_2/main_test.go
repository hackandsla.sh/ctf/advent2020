package main

import (
	"testing"
)

func Test_getCupLabels(t *testing.T) {
	type args struct {
		start string
		size  int
		steps int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			args: args{
				start: "389125467",
				size:  1000000,
				steps: 10000000,
			},
			want: 149245887792,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := getCupLabels(tt.args.start, tt.args.size, tt.args.steps); got != tt.want {
				t.Errorf("getCupLabels() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkCircle_Step(b *testing.B) {
	c, err := NewCircle("389125467", 1000000)
	if err != nil {
		b.Fatal(err)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		c.Step()
	}
}
