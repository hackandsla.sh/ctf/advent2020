package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	lines := strings.Split(string(input), "\n")

	instructions := make([]instruction, len(lines))
	for i, line := range lines {
		action := line[0]
		value, err := strconv.Atoi(line[1:])
		if err != nil {
			return err
		}

		instructions[i] = instruction{
			action: action,
			value:  value,
		}
	}

	north := coordinate{X: 0, Y: 1}
	south := coordinate{X: 0, Y: -1}
	east := coordinate{X: 1, Y: 0}
	west := coordinate{X: -1, Y: 0}

	waypoint := coordinate{X: 10, Y: 1}

	currentCoord := coordinate{X: 0, Y: 0}

	for _, instr := range instructions {
		switch instr.action {
		case 'N':
			waypoint.Y += north.Y * instr.value
		case 'S':
			waypoint.Y += south.Y * instr.value
		case 'E':
			waypoint.X += east.X * instr.value
		case 'W':
			waypoint.X += west.X * instr.value
		case 'F':
			currentCoord.X += waypoint.X * instr.value
			currentCoord.Y += waypoint.Y * instr.value
		case 'R':
			steps := instr.value / 90
			for i := 0; i < steps; i++ {
				waypoint.right()
			}
		case 'L':
			steps := instr.value / 90
			for i := 0; i < steps; i++ {
				waypoint.left()
			}
		}
	}

	distance := 0
	if currentCoord.X < 0 {
		currentCoord.X = -currentCoord.X
	}

	if currentCoord.Y < 0 {
		currentCoord.Y = -currentCoord.Y
	}

	distance = currentCoord.X + currentCoord.Y
	fmt.Println(distance)

	return nil
}

type instruction struct {
	action byte
	value  int
}

type coordinate struct {
	X int
	Y int
}

func (c *coordinate) right() {
	c.X, c.Y = c.Y, -c.X
}

func (c *coordinate) left() {
	c.X, c.Y = -c.Y, c.X
}
