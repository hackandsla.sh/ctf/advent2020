package main

import "testing"

func TestCountNonAllergens(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			args: args{
				input: `mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)`,
			},
			want: "mxmxvkd,sqjhc,fvjkl",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetAllergenList(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("CountNonAllergens() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("CountNonAllergens() = %v, want %v", got, tt.want)
			}
		})
	}
}
