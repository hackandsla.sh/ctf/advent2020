package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"sort"
	"strings"

	"github.com/kr/pretty"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	count, err := GetAllergenList(string(input))
	if err != nil {
		return err
	}

	fmt.Println(count)

	return nil
}

func GetAllergenList(input string) (string, error) {
	lines := strings.Split(string(input), "\n")
	re := regexp.MustCompile(`^(.*) \(contains (.*)\)$`)
	var items []*Item
	for _, line := range lines {
		matches := re.FindStringSubmatch(line)
		if matches == nil {
			return "", fmt.Errorf("couldn't match line '%s'", line)
		}

		items = append(items, &Item{
			Ingredients: strings.Split(strings.TrimSpace(matches[1]), " "),
			Allergens:   strings.Split(strings.TrimSpace(matches[2]), ", "),
		})
	}

	// Maps allergens to their respective candidates
	allergenCandidates := map[string][]string{}

	for _, item := range items {
		for _, allergen := range item.Allergens {
			if candidates, ok := allergenCandidates[allergen]; !ok {
				// This allergen hasn't been seen yet; make all ingredients candidates
				allergenCandidates[allergen] = item.Ingredients
			} else {
				// This allergen has been seen before; get the overlap between
				// this item's candidates and what already exists

				allergenCandidates[allergen] = OverlappingStrings(candidates, item.Ingredients)
			}
		}
	}

	pretty.Println(allergenCandidates)

	for i := 0; i < len(allergenCandidates); i++ {
		for allergen, candidates := range allergenCandidates {
			if len(candidates) == 1 {
				knownAllergen := candidates[0]
				for otherAllergen, otherCandidates := range allergenCandidates {
					if otherAllergen == allergen {
						continue
					}

					// pop the known allergen from the list (if present)
					allergenCandidates[otherAllergen] = PopString(knownAllergen, otherCandidates)
				}
			}
		}
	}

	pretty.Println(allergenCandidates)

	var keys []string
	for k := range allergenCandidates {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	var values []string
	for _, key := range keys {
		values = append(values, allergenCandidates[key][0])
	}

	return strings.Join(values, ","), nil
}

type Item struct {
	Ingredients []string
	Allergens   []string
}

func OverlappingStrings(a, b []string) []string {
	unique := map[string]struct{}{}
	for _, itemA := range a {
		for _, itemB := range b {
			if itemA == itemB {
				unique[itemA] = struct{}{}
			}
		}
	}

	var ret []string
	for k := range unique {
		ret = append(ret, k)
	}

	return ret
}

func PopString(s string, ss []string) []string {
	var ret []string
	for _, item := range ss {
		if item != s {
			ret = append(ret, item)
		}
	}

	return ret
}
