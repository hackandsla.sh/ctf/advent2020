package main

import "testing"

func Test_memoryGame(t *testing.T) {
	type args struct {
		nums        []int
		targetIndex int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			args: args{
				nums:        []int{0, 3, 6},
				targetIndex: 2020,
			},
			want: 436,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := memoryGame(tt.args.nums, tt.args.targetIndex); got != tt.want {
				t.Errorf("memoryGame() = %v, want %v", got, tt.want)
			}
		})
	}
}
