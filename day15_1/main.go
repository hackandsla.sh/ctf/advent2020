package main

import (
	"fmt"
	"log"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	nums := []int{2, 0, 6, 12, 1, 3}

	fmt.Println(memoryGame(nums, 2020))
	return nil
}

func memoryGame(nums []int, targetIndex int) int {
	for {
		if len(nums) > targetIndex {
			break
		}

		nextNum := 0
		lastNum := nums[len(nums)-1]
		for i := len(nums) - 2; i >= 0; i-- {
			if nums[i] == lastNum {
				nextNum = len(nums) - 1 - i
				break
			}
		}

		nums = append(nums, nextNum)
	}

	return nums[targetIndex-1]
}
