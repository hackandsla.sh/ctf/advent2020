package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	sections := strings.Split(string(input), "\n\n")
	t := ticket{
		rules: make(map[string][]*intRange),
	}

	rules := strings.Split(sections[0], "\n")
	rulesRE := regexp.MustCompile(`(.*): (\d+-\d+) or (\d+-\d+)`)
	for _, rule := range rules {
		matches := rulesRE.FindStringSubmatch(rule)
		if matches == nil {
			return errors.New("nil match")
		}

		name := matches[1]
		range1, err := parseIntRange(matches[2])
		if err != nil {
			return err
		}

		range2, err := parseIntRange(matches[3])
		if err != nil {
			return err
		}

		t.rules[name] = []*intRange{range1, range2}
	}

	values := strings.Split(sections[1], "\n")
	t.values, err = parseTicket(values[1])
	if err != nil {
		return err
	}

	neighbors := strings.Split(sections[2], "\n")
	for i := 1; i < len(neighbors); i++ {
		neighbor, err := parseTicket(neighbors[i])
		if err != nil {
			return err
		}

		t.neighbors = append(t.neighbors, neighbor)
	}

	sum := 0
	for _, neighbor := range t.neighbors {
		for _, num := range neighbor {
			isValid := false

		checkIsValid:
			for _, intRanges := range t.rules {
				for _, ir := range intRanges {
					if ir.contains(num) {
						isValid = true
						break checkIsValid
					}
				}
			}

			if !isValid {
				sum += num
			}
		}
	}

	fmt.Println(sum)
	return nil
}

func parseTicket(s string) ([]int, error) {
	split := strings.Split(s, ",")

	ret := make([]int, len(split))
	for i, s := range split {
		parsed, err := strconv.Atoi(s)
		if err != nil {
			return nil, err
		}

		ret[i] = parsed
	}

	return ret, nil
}

type ticket struct {
	rules     map[string][]*intRange
	values    []int
	neighbors [][]int
}

type intRange struct {
	start int
	end   int
}

func parseIntRange(s string) (*intRange, error) {
	split := strings.Split(s, "-")
	start, err := strconv.Atoi(split[0])
	if err != nil {
		return nil, err
	}

	end, err := strconv.Atoi(split[1])
	if err != nil {
		return nil, err
	}

	return &intRange{
		start: start,
		end:   end,
	}, nil
}

func (r *intRange) contains(i int) bool {
	return r.start <= i &&
		r.end >= i
}
