package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	inputs, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	passwords := strings.Split(string(inputs), "\n")

	var policies []policy
	for _, password := range passwords {
		index1, err := strconv.Atoi(password[0:strings.Index(password, "-")])
		if err != nil {
			return err
		}

		index2, err := strconv.Atoi(password[strings.Index(password, "-")+1 : strings.Index(password, " ")])
		if err != nil {
			return err
		}

		char := password[strings.Index(password, " ")+1 : strings.Index(password, " ")+2]

		pass := password[strings.Index(password, ": ")+2:]

		policies = append(policies, policy{
			Index1:   index1,
			Index2:   index2,
			Char:     char,
			Password: pass,
		})
	}

	count := 0
	for _, policy := range policies {
		p1 := string(policy.Password[policy.Index1-1]) == policy.Char
		p2 := string(policy.Password[policy.Index2-1]) == policy.Char

		if (!p1 && p2) || (p1 && !p2) {
			count++
		}
	}

	fmt.Println(count)
	return nil
}

type policy struct {
	Password string
	Index1   int
	Index2   int
	Char     string
}
