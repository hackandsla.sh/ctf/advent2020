package main

import (
	"testing"
)

func Test_getMemSum(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			args: args{
				input: `mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1`,
			},
			want: 208,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getMemSum(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("getMemSum() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getMemSum() = %v, want %v", got, tt.want)
			}
		})
	}
}
