package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	sum, err := getMemSum(string(input))
	if err != nil {
		return err
	}

	fmt.Println(sum)

	return nil
}

func getMemSum(input string) (int, error) {
	lines := strings.Split(input, "\n")

	mask := []rune{}
	mem := map[int]int{}

	for _, line := range lines {
		if strings.HasPrefix(line, "mask = ") {
			mask = make([]rune, 36)
			maskString := strings.TrimPrefix(line, "mask = ")
			for idx, char := range maskString {
				mask[35-idx] = char
			}

			continue
		}

		re := regexp.MustCompile(`mem\[(\d+)\] = (\d+)`)
		matches := re.FindStringSubmatch(line)
		key, err := strconv.Atoi(matches[1])
		if err != nil {
			return 0, err
		}

		value, err := strconv.Atoi(matches[2])
		if err != nil {
			return 0, err
		}

		addrs := getMaskedInts(key, 0, mask)
		for _, addr := range addrs {
			mem[addr] = value
		}
	}

	sum := 0
	for _, v := range mem {
		sum += v
	}

	return sum, nil
}

func getMaskedInts(i, idx int, mask []rune) []int {
	if idx >= len(mask) {
		return []int{i}
	}

	if mask[idx] == '1' {
		m := 1 << idx
		i = i | m
		return getMaskedInts(i, idx+1, mask)
	}

	if mask[idx] == 'X' {
		m := 1 << idx
		i0 := i | m
		i1 := i &^ m
		return append(getMaskedInts(i0, idx+1, mask), getMaskedInts(i1, idx+1, mask)...)
	}

	return getMaskedInts(i, idx+1, mask)
}
