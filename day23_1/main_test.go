package main

import "testing"

func Test_getCupLabels(t *testing.T) {
	type args struct {
		start string
		steps int
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			args: args{
				start: "389125467",
				steps: 10,
			},
			want: "92658374",
		},
		{
			args: args{
				start: "389125467",
				steps: 100,
			},
			want: "67384529",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := getCupLabels(tt.args.start, tt.args.steps); got != tt.want {
				t.Errorf("getCupLabels() = %v, want %v", got, tt.want)
			}
		})
	}
}
