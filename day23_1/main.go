package main

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	result, err := getCupLabels("315679824", 100)
	if err != nil {
		return err
	}

	fmt.Println(result)
	return nil
}

func getCupLabels(start string, steps int) (string, error) {
	var cups Circle
	for _, num := range start {
		cup, err := strconv.Atoi(string(num))
		if err != nil {
			return "", err
		}

		cups = append(cups, cup)
	}

	current := cups[0]
	for i := 0; i < steps; i++ {
		idx := cups.IndexOf(current)

		var next3 []int
		next3, cups = cups.Pop(idx+1, 3)

		insertIndex := cups.TargetIndex(current - 1)

		cups = append(cups[:insertIndex], append(next3, cups[insertIndex:]...)...)

		current = cups.Get(cups.IndexOf(current) + 1)
	}
	oneIndex := cups.IndexOf(1)

	cups = append(cups[oneIndex+1:], cups[:oneIndex]...)

	var cupStrings []string
	for _, cup := range cups {
		cupStrings = append(cupStrings, strconv.Itoa(cup))
	}
	return strings.Join(cupStrings, ""), nil
}

type Circle []int

func (c Circle) Get(index int) int {
	return c[c.Index(index)]
}

func (c Circle) Index(index int) int {
	return index % len(c)
}

func (c Circle) Pop(index int, num int) (popped, circle []int) {
	var ret []int
	for i := 0; i < num; i++ {
		ret = append(ret, c.Get(index+i))
	}

	for _, r := range ret {
		idx := c.IndexOf(r)
		c = append(c[:idx], c[idx+1:]...)
	}

	return ret, c
}

func (c Circle) TargetIndex(target int) int {
	for {
		if target < c.Min() {
			target = c.Max()
		}

		idx := c.IndexOf(target)
		if idx >= 0 {
			return c.Index(idx + 1)
		}
		target--
	}
}

func (c Circle) IndexOf(target int) int {
	for i := 0; i < len(c); i++ {
		if c[i] == target {
			return i
		}
	}

	return -1
}

func (c Circle) Max() int {
	max := 0
	for _, i := range c {
		if i > max {
			max = i
		}
	}

	return max
}

func (c Circle) Min() int {
	min := math.MaxInt64
	for _, i := range c {
		if i < min {
			min = i
		}
	}

	return min
}
