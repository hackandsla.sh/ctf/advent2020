package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	lines := bytes.Split(input, []byte("\n"))

	hitCount := 0
	for i, line := range lines {
		char := line[i*3%len(line)]
		if char == []byte("#")[0] {
			hitCount++
		}
	}

	fmt.Println(hitCount)
	return nil
}
