package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	sections := strings.Split(string(input), "\n\n")
	t := ticket{
		rules: make(map[string][]*intRange),
	}

	rules := strings.Split(sections[0], "\n")
	rulesRE := regexp.MustCompile(`(.*): (\d+-\d+) or (\d+-\d+)`)
	for _, rule := range rules {
		matches := rulesRE.FindStringSubmatch(rule)
		if matches == nil {
			return errors.New("nil match")
		}

		name := matches[1]
		range1, err := parseIntRange(matches[2])
		if err != nil {
			return err
		}

		range2, err := parseIntRange(matches[3])
		if err != nil {
			return err
		}

		t.rules[name] = []*intRange{range1, range2}
	}

	values := strings.Split(sections[1], "\n")
	t.values, err = parseTicket(values[1])
	if err != nil {
		return err
	}

	neighbors := strings.Split(sections[2], "\n")
	for i := 1; i < len(neighbors); i++ {
		neighbor, err := parseTicket(neighbors[i])
		if err != nil {
			return err
		}

		t.neighbors = append(t.neighbors, neighbor)
	}

	var validNeighbors [][]int
	for _, neighbor := range t.neighbors {
		isValid := true

		for _, num := range neighbor {
			numIsValid := false
			for _, intRanges := range t.rules {
				for _, ir := range intRanges {
					if ir.contains(num) {
						numIsValid = true
					}
				}
			}

			if !numIsValid {
				isValid = false
			}
		}

		if isValid {
			validNeighbors = append(validNeighbors, neighbor)
		}
	}

	possibleRuleOrder := make([]map[string]bool, len(validNeighbors[0]))
	for i := 0; i < len(possibleRuleOrder); i++ {
		possibleMatches := map[string]bool{}
		for name := range t.rules {
			possibleMatches[name] = true
		}

		for _, neighbor := range validNeighbors {
			num := neighbor[i]
			for name, rule := range t.rules {
				if !matchesRule(num, rule) {
					delete(possibleMatches, name)
				}
			}
		}

		possibleRuleOrder[i] = possibleMatches
	}

	for {
		exit := true
		for i, possibleRules := range possibleRuleOrder {
			if len(possibleRules) == 1 {
				// if we find that a particular rule can only be one thing,
				// delete that thing from all other rule possibilities.
				var foundRule string
				for rule := range possibleRules {
					foundRule = rule
				}

				for j := 0; j < len(possibleRuleOrder); j++ {
					if j == i {
						continue
					}

					delete(possibleRuleOrder[j], foundRule)
				}
			} else {
				exit = false
			}
		}

		if exit {
			break
		}
	}

	ruleOrder := make([]string, len(possibleRuleOrder))
	for i, rules := range possibleRuleOrder {
		var name string
		for k := range rules {
			name = k
		}

		ruleOrder[i] = name
	}

	product := 1
	for i, num := range t.values {
		if strings.HasPrefix(ruleOrder[i], "departure") {
			product *= num
		}
	}

	fmt.Println(product)
	return nil
}

func parseTicket(s string) ([]int, error) {
	split := strings.Split(s, ",")

	ret := make([]int, len(split))
	for i, s := range split {
		parsed, err := strconv.Atoi(s)
		if err != nil {
			return nil, err
		}

		ret[i] = parsed
	}

	return ret, nil
}

type ticket struct {
	rules     map[string][]*intRange
	values    []int
	neighbors [][]int
}

type intRange struct {
	start int
	end   int
}

func matchesRule(i int, rule []*intRange) bool {
	for _, r := range rule {
		if r.contains(i) {
			return true
		}
	}

	return false
}

func parseIntRange(s string) (*intRange, error) {
	split := strings.Split(s, "-")
	start, err := strconv.Atoi(split[0])
	if err != nil {
		return nil, err
	}

	end, err := strconv.Atoi(split[1])
	if err != nil {
		return nil, err
	}

	return &intRange{
		start: start,
		end:   end,
	}, nil
}

func (r *intRange) contains(i int) bool {
	return r.start <= i &&
		r.end >= i
}
