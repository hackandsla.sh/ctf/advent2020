package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	count := getYesses(input)

	fmt.Println(count)
	return nil
}

func getYesses(input []byte) int {
	groups := bytes.Split(input, []byte("\n\n"))

	maps := make([][]map[byte]struct{}, len(groups))
	for i, group := range groups {
		people := bytes.Split(group, []byte("\n"))

		for _, person := range people {
			personMap := map[byte]struct{}{}

			for _, item := range person {
				personMap[item] = struct{}{}
			}

			maps[i] = append(maps[i], personMap)
		}
	}

	count := 0
	for _, group := range maps {
		first := group[0]
		for char := range first {
			allYes := true
			for i := 1; i < len(group); i++ {
				if _, ok := group[i][char]; !ok {
					allYes = false
				}
			}

			if allYes {
				count++
			}
		}

	}

	return count
}
