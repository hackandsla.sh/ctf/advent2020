package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"github.com/kr/pretty"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	num := numFlippedTiles(string(input))
	fmt.Println(num)
	return nil
}

func numFlippedTiles(input string) int {
	var directions [][]Coord
	lines := strings.Split(input, "\n")
	for _, line := range lines {
		var path []Coord
		for i := 0; i < len(line); i++ {
			var coord Coord
			char := line[i]
			if char == 's' || char == 'n' {
				i++
				horizontalChar := line[i]
				switch char {
				case 's':
					coord.Y = -1
				case 'n':
					coord.Y = 1
				}

				switch horizontalChar {
				case 'e':
					coord.X = 1
				case 'w':
					coord.X = -1
				}
			} else {
				switch char {
				case 'e':
					coord.X = 1
				case 'w':
					coord.X = -1
				}
			}

			path = append(path, coord)
		}

		directions = append(directions, path)
	}

	var blackTiles []Coord
	for _, path := range directions {
		current := Coord{}
		for _, vector := range path {
			if current.Y%2 == 0 && vector.Y != 0 && vector.X == -1 {
				vector.X = 0
			}
			if current.Y%2 != 0 && vector.Y != 0 && vector.X == 1 {
				vector.X = 0
			}

			current.X += vector.X
			current.Y += vector.Y
		}

		foundMatch := -1
		for i, tile := range blackTiles {
			if tile.X == current.X && tile.Y == current.Y {
				foundMatch = i
				break
			}
		}

		if foundMatch == -1 {
			blackTiles = append(blackTiles, current)
		} else {
			blackTiles = append(blackTiles[:foundMatch], blackTiles[foundMatch+1:]...)
		}
	}

	pretty.Println(directions)
	return len(blackTiles)
}

type Coord struct {
	X int
	Y int
}
