package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	var tiles []*Tile

	tileInputs := bytes.Split(input, []byte("\n\n"))
	for _, tileInput := range tileInputs {
		rows := bytes.Split(tileInput, []byte("\n"))
		id, err := strconv.Atoi(string(rows[0][5:9])) // Extract just the ID number from the "Tile 1234:" header
		if err != nil {
			return err
		}

		tiles = append(tiles, &Tile{
			ID:     id,
			Pixels: rows[1:],
		})
	}

	reconstructed := make([][]*Tile, 12)
	// for i := range reconstructed {
	// 	reconstructed[i] = make([]*Tile, len(reconstructed))
	// }

	reconstructed[0] = append(reconstructed[0], tiles[0])
	tiles = tiles[1:]

	// Get the right-side blocks to the anchor
horizontal:
	for {
		right := reconstructed[0][len(reconstructed[0])-1]
		left := reconstructed[0][0]

		for i, tile := range tiles {
			permutations := tile.GetPermutations()
			for _, permutation := range permutations {
				if bytes.Equal(right.Right(), permutation.Left()) {
					reconstructed[0] = append(reconstructed[0], permutation)
					// pop out our found tile
					tiles = append(tiles[0:i], tiles[i+1:]...)

					continue horizontal
				}

				if bytes.Equal(left.Left(), permutation.Right()) {
					reconstructed[0] = append([]*Tile{permutation}, reconstructed[0]...)
					// pop out our found tile
					tiles = append(tiles[0:i], tiles[i+1:]...)

					continue horizontal
				}
			}
		}

		break
	}

	for y := 1; y < len(reconstructed); y++ {
		reconstructed[y] = make([]*Tile, len(reconstructed))
	}

	for x := range reconstructed[0] {
	vertical:
		for y := 0; y < len(reconstructed)-1; y++ {
			top := reconstructed[0][x]
			bottom := reconstructed[y][x]

			for i, tile := range tiles {
				permutations := tile.GetPermutations()
				for _, permutation := range permutations {
					if bytes.Equal(top.Top(), permutation.Bottom()) {
						// pop out our found tile
						tiles = append(tiles[0:i], tiles[i+1:]...)

						// Shift the column down 1
						for j := len(reconstructed) - 1; j > 0; j-- {
							reconstructed[j][x] = reconstructed[j-1][x]
						}

						// Prepend our permutation
						reconstructed[0][x] = permutation

						continue vertical
					}

					if bytes.Equal(bottom.Bottom(), permutation.Top()) {
						// pop out our found tile
						tiles = append(tiles[0:i], tiles[i+1:]...)

						reconstructed[y+1][x] = permutation

						continue vertical
					}
				}
			}
		}
	}

	image := Image(reconstructed)

	targetInput, err := ioutil.ReadFile("monster.txt")
	if err != nil {
		return err
	}

	target := bytes.Split(targetInput, []byte("\n"))
	var numMonsters int
	for _, permutation := range image.GetPermutations() {
		numFound := permutation.findSubImages(target)
		if numFound != 0 {
			numMonsters = numFound
		}
	}

	charsPerMonster := findChar(target, '#')
	waterRoughness := findChar(image.Pixels, '#') - numMonsters*charsPerMonster
	fmt.Println(waterRoughness)

	return nil
}

func findChar(image [][]byte, char byte) int {
	var count int
	for _, row := range image {
		for _, pixel := range row {
			if pixel == char {
				count++
			}
		}
	}

	return count
}

func (t *Tile) findSubImages(sub [][]byte) int {
	var count int
	for y, row := range t.Pixels {
		for x := range row {
			foundMatch := true
			for subY, subRow := range sub {
				for subX, subPixel := range subRow {
					if subPixel != '#' {
						continue
					}

					if y+subY >= len(t.Pixels) {
						foundMatch = false
						continue
					}

					if x+subX >= len(t.Pixels[y]) {
						foundMatch = false
						continue
					}

					if t.Pixels[y+subY][x+subX] != '#' {
						foundMatch = false
					}
				}
			}

			if foundMatch {
				count++
			}
		}
	}

	return count
}

func Image(tiles [][]*Tile) *Tile {
	ret := &Tile{}

	for _, row := range tiles {
		for i := 1; i < len(row[0].Pixels)-1; i++ {
			var combinedRow []byte
			for _, tile := range row {
				combinedRow = append(combinedRow, tile.Pixels[i][1:len(tile.Pixels[i])-1]...)
			}

			ret.Pixels = append(ret.Pixels, combinedRow)
		}
	}

	return ret
}

func PrintTiles(tiles [][]*Tile) string {
	var builder strings.Builder

	for _, row := range tiles {
		if len(row) == 0 {
			continue
		}

		for i := 0; i < len(row[0].Pixels); i++ {
			for _, tile := range row {
				builder.Write(tile.Pixels[i])
				builder.WriteByte(' ')
			}
			builder.WriteByte('\n')
		}

		builder.WriteByte('\n')
	}

	return builder.String()
}

type Tile struct {
	ID     int
	Pixels [][]byte
}

func (t *Tile) Right() []byte {
	var right []byte

	for y := 0; y < len(t.Pixels); y++ {
		right = append(right, t.Pixels[y][len(t.Pixels[y])-1])
	}

	return right
}

func (t *Tile) Left() []byte {
	var left []byte

	for y := 0; y < len(t.Pixels); y++ {
		left = append(left, t.Pixels[y][0])
	}

	return left
}

func (t *Tile) Top() []byte {
	return t.Pixels[0]
}

func (t *Tile) Bottom() []byte {
	return t.Pixels[len(t.Pixels)-1]
}

// Flip flips the image horizontally
func (t *Tile) Flip() *Tile {
	ret := &Tile{
		ID: t.ID,
	}

	ret.Pixels = make([][]byte, len(t.Pixels))
	for y, row := range t.Pixels {
		ret.Pixels[y] = make([]byte, len(row))
		for x, pixel := range row {
			ret.Pixels[y][len(row)-1-x] = pixel
		}
	}

	return ret
}

// Rotate rotates the tile 90 degrees right
func (t *Tile) Rotate() *Tile {
	ret := &Tile{
		ID: t.ID,
	}

	ret.Pixels = make([][]byte, len(t.Pixels))
	for y, row := range t.Pixels {
		ret.Pixels[y] = make([]byte, len(row))
	}

	for y, row := range t.Pixels {
		for x, pixel := range row {
			ret.Pixels[x][len(row)-1-y] = pixel
		}
	}

	return ret
}

func (t *Tile) String() string {
	var builder strings.Builder

	for _, row := range t.Pixels {
		builder.Write(row)
		builder.WriteByte('\n')
	}

	return builder.String()
}

func (t *Tile) GetPermutations() []*Tile {
	var permutations []*Tile

	for i := 0; i < 4; i++ {
		t = t.Rotate()
		permutations = append(permutations, t)
	}
	t = t.Flip()

	for i := 0; i < 4; i++ {
		t = t.Rotate()
		permutations = append(permutations, t)
	}

	return permutations
}
