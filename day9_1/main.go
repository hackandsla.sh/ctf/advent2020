package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	lines := strings.Split(string(input), "\n")

	var nums []int
	for _, line := range lines {
		num, err := strconv.Atoi(line)
		if err != nil {
			return err
		}

		nums = append(nums, num)
	}

	for i := 25; i < len(nums); i++ {
		foundSum := false
		for j := i - 25; j < i; j++ {
			for k := i - 25; k < i; k++ {
				if nums[j] == nums[k] {
					continue
				}

				if nums[j]+nums[k] == nums[i] {
					foundSum = true
				}
			}
		}

		if !foundSum {
			fmt.Printf("Found '%d' at position '%d' without a valid sum\n", nums[i], i)
		}
	}
	return nil
}
