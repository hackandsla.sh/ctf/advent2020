package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	size := 100
	space := init3DSpace(size)

	lines := strings.Split(string(input), "\n")
	xStart := size/2 - (len(lines[0]) / 2)
	yStart := size/2 - (len(lines) / 2)
	zStart := size / 2
	for y, line := range lines {
		for x, char := range line {
			if char == '#' {
				space[xStart+x][yStart+y][zStart] = true
			}
		}
	}

	for i := 0; i < 6; i++ {
		space = step(space)
	}

	fmt.Println(count(space))
	return nil
}

func init3DSpace(i int) [][][]bool {
	space := make([][][]bool, i)
	for x := 0; x < len(space); x++ {
		space[x] = make([][]bool, i)
		for y := 0; y < len(space[x]); y++ {
			space[x][y] = make([]bool, i)
		}
	}

	return space
}

func count(space [][][]bool) int {
	count := 0
	for x := 0; x < len(space); x++ {
		for y := 0; y < len(space[x]); y++ {
			for z := 0; z < len(space[x][y]); z++ {
				if space[x][y][z] {
					count++
				}
			}
		}
	}

	return count
}

func step(space [][][]bool) [][][]bool {
	next := init3DSpace(len(space))

	for x := 0; x < len(space); x++ {
		for y := 0; y < len(space[x]); y++ {
			for z := 0; z < len(space[x][y]); z++ {
				numNeighbors := numActiveNeighbors(x, y, z, space)
				if space[x][y][z] && numNeighbors >= 2 && numNeighbors <= 3 {
					next[x][y][z] = true
				}

				if !space[x][y][z] && numNeighbors == 3 {
					next[x][y][z] = true
				}
			}
		}
	}

	return next
}

func numActiveNeighbors(x, y, z int, space [][][]bool) int {
	count := 0
	for xIdx := x - 1; xIdx <= x+1; xIdx++ {
		if xIdx < 0 || xIdx >= len(space) {
			continue
		}

		for yIdx := y - 1; yIdx <= y+1; yIdx++ {
			if yIdx < 0 || yIdx >= len(space[x]) {
				continue
			}

			for zIdx := z - 1; zIdx <= z+1; zIdx++ {
				if zIdx < 0 || zIdx >= len(space[x][y]) {
					continue
				}

				if xIdx == x && yIdx == y && zIdx == z {
					// Skip the cell being evaluated
					continue
				}

				if space[xIdx][yIdx][zIdx] {
					count++
				}
			}
		}
	}

	return count
}
