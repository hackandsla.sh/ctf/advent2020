package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	lines := strings.Split(string(input), "\n")

	instructions := make([]instruction, len(lines))
	for i, line := range lines {
		action := line[0]
		value, err := strconv.Atoi(line[1:])
		if err != nil {
			return err
		}

		instructions[i] = instruction{
			action: action,
			value:  value,
		}
	}

	north := coordinate{X: 0, Y: -1}
	south := coordinate{X: 0, Y: 1}
	east := coordinate{X: 1, Y: 0}
	west := coordinate{X: -1, Y: 0}

	compass := []coordinate{
		north, east, south, west,
	}

	currentDir := 1

	currentCoord := coordinate{X: 0, Y: 0}

	for _, instr := range instructions {
		switch instr.action {
		case 'N':
			currentCoord.Y += north.Y * instr.value
		case 'S':
			currentCoord.Y += south.Y * instr.value
		case 'E':
			currentCoord.X += east.X * instr.value
		case 'W':
			currentCoord.X += west.X * instr.value
		case 'F':
			currentCoord.X += compass[currentDir].X * instr.value
			currentCoord.Y += compass[currentDir].Y * instr.value
		case 'R':
			steps := instr.value / 90
			currentDir = (currentDir + steps) % len(compass)
		case 'L':
			steps := instr.value / 90
			currentDir = (currentDir + len(compass) - steps) % len(compass)
		}
	}

	distance := 0
	if currentCoord.X < 0 {
		currentCoord.X = -currentCoord.X
	}

	if currentCoord.Y < 0 {
		currentCoord.Y = -currentCoord.Y
	}

	distance = currentCoord.X + currentCoord.Y
	fmt.Println(distance)

	return nil
}

type instruction struct {
	action byte
	value  int
}

type coordinate struct {
	X int
	Y int
}
