package main

import (
	"testing"
)

func Test_getRowNum(t *testing.T) {
	type args struct {
		row []byte
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			args: args{
				row: []byte("FBFBBFFRLR"),
			},
			want: 44,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getRowNum(tt.args.row); got != tt.want {
				t.Errorf("getColumnNum() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getColumnNum(t *testing.T) {
	type args struct {
		column []byte
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			args: args{
				column: []byte("RLR"),
			},
			want: 5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getColumnNum(tt.args.column); got != tt.want {
				t.Errorf("getRowNum() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getSeatID(t *testing.T) {
	type args struct {
		seat []byte
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			args: args{
				seat: []byte("BFFFBBFRRR"),
			},
			want: 567,
		},
		{
			args: args{
				seat: []byte("FFFBBBFRRR"),
			},
			want: 119,
		},
		{
			args: args{
				seat: []byte("BBFFBBFRLL"),
			},
			want: 820,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getSeatID(tt.args.seat); got != tt.want {
				t.Errorf("getSeatID() = %v, want %v", got, tt.want)
			}
		})
	}
}
