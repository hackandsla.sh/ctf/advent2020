package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"sort"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	var ids []int
	seats := bytes.Split(input, []byte("\n"))
	for _, seat := range seats {
		ids = append(ids, getSeatID(seat))
	}

	sort.Ints(ids)
	fmt.Printf("Max ID: %d\n", ids[len(ids)-1])

	return nil
}

func getRowNum(row []byte) int {
	max := 127
	min := 0
	for _, char := range row {
		if char == 'F' {
			max = max - (max-min+1)/2
		} else if char == 'B' {
			min = min + (max-min+1)/2
		}
	}

	return min
}

func getColumnNum(column []byte) int {
	max := 7
	min := 0
	for _, char := range column {
		if char == 'L' {
			max = max - (max-min+1)/2
		} else if char == 'R' {
			min = min + (max-min+1)/2
		}
	}

	return min
}

func calculateID(column, row int) int {
	return row*8 + column
}

func getSeatID(seat []byte) int {
	rowNum := getRowNum(seat[0:7])
	columnNum := getColumnNum(seat[7:])

	return calculateID(columnNum, rowNum)
}
