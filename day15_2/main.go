package main

import (
	"fmt"
	"log"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	nums := []int{2, 0, 6, 12, 1, 3}

	fmt.Println(memoryGame(nums, 30000000))
	return nil
}

func memoryGame(start []int, targetIndex int) int {
	// nums maps a particular number to the index it was last seen on
	nums := map[int]int{}
	for i, v := range start {
		nums[v] = i
	}

	index := len(start)
	nextNum := 0

	for {
		if index >= targetIndex-1 {
			return nextNum
		}

		if lastSeen, ok := nums[nextNum]; ok {
			nums[nextNum] = index
			nextNum = index - lastSeen
		} else {
			nums[nextNum] = index
			nextNum = 0
		}

		index++
	}
}
