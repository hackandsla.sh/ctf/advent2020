package main

import "testing"

func Test_brute(t *testing.T) {
	type args struct {
		publicKey int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			args: args{
				publicKey: 5764801,
			},
			want: 8,
		},
		{
			args: args{
				publicKey: 17807724,
			},
			want: 11,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := brute(tt.args.publicKey); got != tt.want {
				t.Errorf("brute() = %v, want %v", got, tt.want)
			}
		})
	}
}
