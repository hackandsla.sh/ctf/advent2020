package main

import (
	"fmt"
	"log"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	doorKey := 8421034
	cardKey := 15993936

	doorLoopSize := brute(doorKey)
	fmt.Println()
	fmt.Printf("loop size: %d\n", doorLoopSize)
	fmt.Println(transform(doorLoopSize, cardKey))

	cardLoopSize := brute(cardKey)
	fmt.Println()
	fmt.Printf("loop size: %d\n", cardLoopSize)
	fmt.Println(transform(cardLoopSize, doorKey))

	return nil
}

func brute(publicKey int) int {
	var loopSize int

	num := 1

	for {
		loopSize++
		num *= 7
		num %= 20201227
		if publicKey == num {
			return loopSize
		}
	}
}

func transform(loopSize, subjectNumber int) int {
	num := 1
	for i := 0; i < loopSize; i++ {
		num *= subjectNumber
		num %= 20201227
	}
	return num
}
