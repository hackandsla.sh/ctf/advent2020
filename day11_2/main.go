package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	seats := bytes.Split(input, []byte{'\n'})

	occupied := getStableSeatCount(seats)

	fmt.Println(occupied)
	return nil
}

type coordinate struct {
	X int
	Y int
}

func getStableSeatCount(seats [][]byte) int {
	for {
		// print2DBytes(seats)
		// fmt.Println()

		next := step(seats)
		if compare2DBytes(next, seats) {
			break
		}

		seats = next
	}

	occupied := 0
	for _, row := range seats {
		for _, seat := range row {
			if seat == '#' {
				occupied++
			}
		}
	}
	return occupied
}

func print2DBytes(rows [][]byte) {
	for _, row := range rows {
		fmt.Println(string(row))
	}
}

func compare2DBytes(b1, b2 [][]byte) bool {
	for r := range b1 {
		for c := range b1 {
			if b1[r][c] != b2[r][c] {
				return false
			}
		}
	}

	return true
}

func step(seats [][]byte) [][]byte {
	// Create a deep copy of our state
	next := make([][]byte, len(seats))
	for i, row := range seats {
		next[i] = make([]byte, len(row))
		copy(next[i], row)
	}

	for r, row := range seats {
		for c, seat := range row {
			if seat == '.' {
				continue
			}

			adjacents := getLOSSeats(seats, coordinate{Y: r, X: c})

			if seat == 'L' && adjacents == 0 {
				next[r][c] = '#'
			}

			if seat == '#' && adjacents >= 5 {
				next[r][c] = 'L'
			}
		}
	}

	return next
}

func getLOSSeats(seats [][]byte, coord coordinate) int {
	vectors := []coordinate{
		{X: -1, Y: -1},
		{X: 0, Y: -1},
		{X: 1, Y: -1},
		{X: -1, Y: 0},
		{X: 1, Y: 0},
		{X: -1, Y: 1},
		{X: 0, Y: 1},
		{X: 1, Y: 1},
	}

	count := 0
	for _, vector := range vectors {
		current := coordinate{X: coord.X, Y: coord.Y}
		for {
			current.X += vector.X
			current.Y += vector.Y

			if current.X < 0 || current.Y < 0 {
				break
			}

			if current.Y >= len(seats) || current.X >= len(seats[current.Y]) {
				break
			}

			if seats[current.Y][current.X] == 'L' {
				break
			}

			if seats[current.Y][current.X] == '#' {
				count++
				break
			}
		}
	}

	return count
}
