package main

import (
	"strings"
	"testing"
)

func getSeats(s string) [][]byte {
	split := strings.Split(s, "\n")

	var ret [][]byte
	for _, line := range split {
		ret = append(ret, []byte(line))
	}

	return ret
}

func Test_getLOSSeats(t *testing.T) {

	type args struct {
		seats [][]byte
		coord coordinate
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			args: args{
				coord: coordinate{X: 3, Y: 4},
				seats: getSeats(`.......#.
...#.....
.#.......
.........
..#L....#
....#....
.........
#........
...#.....`,
				),
			},
			want: 8,
		},
		{
			args: args{
				coord: coordinate{X: 1, Y: 1},
				seats: getSeats(`.............
.L.L.#.#.#.#.
.............`,
				),
			},
			want: 0,
		},
		{
			args: args{
				coord: coordinate{X: 3, Y: 3},
				seats: getSeats(`.##.##.
#.#.#.#
##...##
...L...
##...##
#.#.#.#
.##.##.`,
				),
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getLOSSeats(tt.args.seats, tt.args.coord); got != tt.want {
				t.Errorf("getLOSSeats() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getStableSeatCount(t *testing.T) {
	type args struct {
		seats [][]byte
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			args: args{
				seats: getSeats(`L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL`),
			},
			want: 26,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getStableSeatCount(tt.args.seats); got != tt.want {
				t.Errorf("getStableSeatCount() = %v, want %v", got, tt.want)
			}
		})
	}
}
