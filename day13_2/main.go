package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
	"time"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	t, err := getBusTime(string(input))
	if err != nil {
		return err
	}

	fmt.Println()
	fmt.Println(t)

	return nil
}

func getBusTime(input string) (t int64, err error) {
	lines := strings.Split(input, "\n")

	busStrings := strings.Split(lines[0], ",")
	buses := make([]int64, len(busStrings))
	for i, s := range busStrings {
		if s == "x" {
			buses[i] = 0
			continue
		}

		bus, err := strconv.Atoi(s)
		if err != nil {
			return 0, err
		}

		buses[i] = int64(bus)
	}

	maxBus := int64(0)
	maxIdx := 0

	for i, bus := range buses {
		if bus > maxBus {
			maxBus = bus
			maxIdx = i
		}
	}

	i := maxBus - int64(maxIdx)

	done := make(chan bool)
	ticker := time.NewTicker(10 * time.Second)

	go func() {
		p := message.NewPrinter(language.English)
		for {
			select {
			case <-done:
				ticker.Stop()
				return
			case <-ticker.C:
				p.Printf("\rCurrent index: %d", i)
			}
		}
	}()

	for {
		foundTime := true
		for j := int64(0); j < int64(len(buses)); j++ {
			if buses[j] == 0 {
				continue
			}

			if (i+j)%buses[j] != 0 {
				foundTime = false
				break
			}
		}

		if foundTime {
			break
		}

		i += maxBus
	}

	close(done)
	return i, nil
}
