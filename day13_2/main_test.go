package main

import "testing"

func Test_getBusTime(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		wantT   int64
		wantErr bool
	}{
		{
			args: args{
				input: "17,x,13,19",
			},
			wantT: 3417,
		},
		{
			args: args{
				input: "67,7,59,61",
			},
			wantT: 754018,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotT, err := getBusTime(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("getBusTime() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotT != tt.wantT {
				t.Errorf("getBusTime() = %v, want %v", gotT, tt.wantT)
			}
		})
	}
}
