package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	count, err := getValidPassports(string(input))
	if err != nil {
		return err
	}

	fmt.Println(count)
	return nil
}

func getValidPassports(input string) (int, error) {
	split := strings.Split(string(input), "\n\n")
	// fmt.Println(strings.Join(split, " | "))

	fmt.Println(len(split))
	var ps []passport
	for _, s := range split {
		p := passport{}
		lines := strings.Split(s, "\n")
		for _, line := range lines {
			kvs := strings.Split(line, " ")
			for _, kv := range kvs {
				if kv == "" {
					break
				}
				kvSplit := strings.Split(kv, ":")
				// fmt.Println(kvSplit)
				key, value := kvSplit[0], kvSplit[1]
				switch key {
				case "byr":
					p.byr = value
				case "iyr":
					p.iyr = value
				case "eyr":
					p.eyr = value
				case "hgt":
					p.hgt = value
				case "hcl":
					p.hcl = value
				case "ecl":
					p.ecl = value
				case "pid":
					p.pid = value
				case "cid":
					p.cid = value
				default:
					fmt.Printf("unknown key: %s\n", key)
				}
			}
		}

		ps = append(ps, p)
	}

	count := 0
	for _, p := range ps {
		if err := p.Validate(); err == nil {
			count++
		}
	}

	return count, nil
}

type passport struct {
	byr string
	iyr string
	eyr string
	hgt string
	hcl string
	ecl string
	pid string
	cid string
}

func (p *passport) Validate() error {
	return validation.ValidateStruct(p,
		validation.Field(&p.byr, validation.Required, validation.By(validateStringInt(1920, 2002))),
		validation.Field(&p.iyr, validation.Required, validation.By(validateStringInt(2010, 2020))),
		validation.Field(&p.hgt, validation.Required, validation.By(validateHeight)),
		validation.Field(&p.eyr, validation.Required, validation.By(validateStringInt(2020, 2030))),
		validation.Field(&p.hcl, validation.Required, validation.Match(regexp.MustCompile(`^\#[0-9a-f]{6}$`))),
		validation.Field(&p.ecl, validation.Required, validation.In("amb", "blu", "brn", "gry", "grn", "hzl", "oth")),
		validation.Field(&p.pid, validation.Required, validation.Match(regexp.MustCompile(`^\d{9}$`))),
	)
}

func validateHeight(i interface{}) error {
	s, ok := i.(string)
	if !ok {
		return fmt.Errorf("%s not a string", i)
	}

	if strings.HasSuffix(s, "in") {
		s = strings.TrimSuffix(s, "in")
		i, err := strconv.Atoi(s)
		if err != nil {
			return err
		}

		if i < 59 || i > 76 {
			return fmt.Errorf("height %din out of range", i)
		}
	} else if strings.HasSuffix(s, "cm") {
		s = strings.TrimSuffix(s, "cm")
		i, err := strconv.Atoi(s)
		if err != nil {
			return err
		}

		if i < 150 || i > 193 {
			return fmt.Errorf("height %dcm out of range", i)
		}
	} else {
		return fmt.Errorf("unknown units '%s'", s)
	}

	return nil
}

func validateStringInt(min, max int) validation.RuleFunc {
	return func(value interface{}) error {
		s, ok := value.(string)
		if !ok {
			return fmt.Errorf("%v is not a string", value)
		}

		i, err := strconv.Atoi(s)
		if err != nil {
			return err
		}

		if i < min || i > max {
			return fmt.Errorf("%d outside of range %d-%d", i, min, max)
		}

		return nil
	}
}
