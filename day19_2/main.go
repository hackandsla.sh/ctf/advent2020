package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	sections := strings.Split(string(input), "\n\n")
	// messageInput := strings.Split(sections[1], "\n")

	rules, err := parseRules(sections[0])
	if err != nil {
		return err
	}

	messages := strings.Split(sections[1], "\n")
	var count int
	for _, message := range messages {
		if MatchesRule(message, rules) {
			count++
		}
	}

	fmt.Println(count)
	return nil
}

type rule struct {
	chars    byte
	subRules [][]int
}

func parseRules(input string) (map[int]rule, error) {
	split := strings.Split(input, "\n")

	rules := map[int]rule{}
	for _, r := range split {
		index, err := strconv.Atoi(r[:strings.Index(r, ":")])
		if err != nil {
			return nil, err
		}

		content := r[strings.Index(r, ":")+2:]
		if strings.Contains(content, "\"") {
			rules[index] = rule{
				chars: content[1],
			}
		} else {
			split := strings.Split(content, "|")
			var subs [][]int
			for _, s := range split {
				s = strings.TrimSpace(s)
				nums := strings.Split(s, " ")
				var sub []int
				for _, n := range nums {
					num, err := strconv.Atoi(n)
					if err != nil {
						return nil, err
					}
					sub = append(sub, num)
				}

				subs = append(subs, sub)
			}

			rules[index] = rule{
				subRules: subs,
			}
		}
	}
	return rules, nil
}

func MatchesRule(s string, rules map[int]rule) bool {
	matches, length := matchesRule(s, rules[0], rules, 0)
	return matches && length == len(s)
}

func matchesRule(s string, r rule, rules map[int]rule, index int) (matches bool, length int) {
	if r.chars != 0 {
		// Return false if we have no more chars to match against.
		if index >= len(s) {
			return false, 0
		}

		return s[index] == r.chars, 1
	}

	for _, subRule := range r.subRules {
		index := index
		var totalLength int
		foundMatch := true
		for _, subIndex := range subRule {
			if isMatch, length := matchesRule(s, rules[subIndex], rules, index); isMatch {
				index += length
				totalLength += length
			} else {
				foundMatch = false
				break
			}
		}

		if foundMatch {
			return true, totalLength
		}
	}

	return false, 0
}
