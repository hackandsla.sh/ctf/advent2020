package main

import "testing"

func TestMatchesRule(t *testing.T) {
	testRules, err := parseRules(`42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: "a"
11: 42 31 | 42 11 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: "b"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42 | 42 8
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1`)
	if err != nil {
		t.Fatal(err)
	}

	type args struct {
		s     string
		rules map[int]rule
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			args: args{
				s:     "babbbbaabbbbbabbbbbbaabaaabaaa",
				rules: testRules,
			},
			want: true,
		},
		{
			args: args{
				s:     "bbabbbbaabaabba",
				rules: testRules,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MatchesRule(tt.args.s, tt.args.rules); got != tt.want {
				t.Errorf("MatchesRule() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_matchesRule(t *testing.T) {
	type args struct {
		s     string
		r     rule
		rules map[int]rule
		index int
	}
	tests := []struct {
		name        string
		args        args
		wantMatches bool
		wantLength  int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotMatches, gotLength := matchesRule(tt.args.s, tt.args.r, tt.args.rules, tt.args.index)
			if gotMatches != tt.wantMatches {
				t.Errorf("matchesRule() gotMatches = %v, want %v", gotMatches, tt.wantMatches)
			}
			if gotLength != tt.wantLength {
				t.Errorf("matchesRule() gotLength = %v, want %v", gotLength, tt.wantLength)
			}
		})
	}
}
