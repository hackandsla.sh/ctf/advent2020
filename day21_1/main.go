package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	count, err := CountNonAllergens(string(input))
	if err != nil {
		return err
	}

	fmt.Println(count)

	return nil
}

func CountNonAllergens(input string) (int, error) {
	lines := strings.Split(string(input), "\n")
	re := regexp.MustCompile(`^(.*) \(contains (.*)\)$`)
	var items []*Item
	for _, line := range lines {
		matches := re.FindStringSubmatch(line)
		if matches == nil {
			return 0, fmt.Errorf("couldn't match line '%s'", line)
		}

		items = append(items, &Item{
			Ingredients: strings.Split(strings.TrimSpace(matches[1]), " "),
			Allergens:   strings.Split(strings.TrimSpace(matches[2]), ", "),
		})
	}

	// Maps allergens to their respective candidates
	allergenCandidates := map[string][]string{}

	for _, item := range items {
		for _, allergen := range item.Allergens {
			if candidates, ok := allergenCandidates[allergen]; !ok {
				// This allergen hasn't been seen yet; make all ingredients candidates
				allergenCandidates[allergen] = item.Ingredients
			} else {
				// This allergen has been seen before; get the overlap between
				// this item's candidates and what already exists

				allergenCandidates[allergen] = OverlappingStrings(candidates, item.Ingredients)
			}
		}
	}

	allAllergenCandidates := map[string]bool{}
	for _, ingredients := range allergenCandidates {
		for _, ingredient := range ingredients {
			allAllergenCandidates[ingredient] = true
		}
	}

	var count int
	for _, item := range items {
		for _, ingredient := range item.Ingredients {
			if !allAllergenCandidates[ingredient] {
				count++
			}
		}
	}

	return count, nil
}

type Item struct {
	Ingredients []string
	Allergens   []string
}

func OverlappingStrings(a, b []string) []string {
	unique := map[string]struct{}{}
	for _, itemA := range a {
		for _, itemB := range b {
			if itemA == itemB {
				unique[itemA] = struct{}{}
			}
		}
	}

	var ret []string
	for k := range unique {
		ret = append(ret, k)
	}

	return ret
}
