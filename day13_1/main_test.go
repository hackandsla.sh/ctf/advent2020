package main

import "testing"

func Test_getMinWaitTime(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name         string
		args         args
		wantWaitTime int
		wantId       int
		wantErr      bool
	}{
		{
			args: args{
				input: `939
7,13,x,x,59,x,31,19`,
			},
			wantWaitTime: 5,
			wantId:       59,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotWaitTime, gotId, err := getMinWaitTime(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("getMinWaitTime() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotWaitTime != tt.wantWaitTime {
				t.Errorf("getMinWaitTime() gotWaitTime = %v, want %v", gotWaitTime, tt.wantWaitTime)
			}
			if gotId != tt.wantId {
				t.Errorf("getMinWaitTime() gotId = %v, want %v", gotId, tt.wantId)
			}
		})
	}
}
