package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	waitTime, id, err := getMinWaitTime(string(input))
	if err != nil {
		return err
	}

	fmt.Println(waitTime * id)

	return nil
}

func getMinWaitTime(input string) (waitTime, id int, err error) {
	lines := strings.Split(input, "\n")
	earliestTime, err := strconv.Atoi(lines[0])
	if err != nil {
		return 0, 0, err
	}

	busStrings := strings.Split(lines[1], ",")
	var buses []int
	for _, s := range busStrings {
		if s == "x" {
			continue
		}

		bus, err := strconv.Atoi(s)
		if err != nil {
			return 0, 0, err
		}

		buses = append(buses, bus)
	}

	minWaitID := 99999999999
	minWait := 99999999999

	for _, bus := range buses {
		wait := earliestTime/bus*bus + bus - earliestTime
		if wait < minWait {
			minWait = wait
			minWaitID = bus
		}
	}

	return minWait, minWaitID, nil
}
