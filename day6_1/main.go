package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	count := getYesses(input)

	fmt.Println(count)
	return nil
}

func getYesses(input []byte) int {
	var maps []map[byte]struct{}
	groups := bytes.Split(input, []byte("\n\n"))
	for _, group := range groups {
		m := map[byte]struct{}{}
		people := bytes.Split(group, []byte("\n"))

		for _, person := range people {
			for _, item := range person {
				m[item] = struct{}{}
			}
		}

		maps = append(maps, m)
	}

	count := 0
	for _, m := range maps {
		for range m {
			count++
		}
	}

	return count
}
