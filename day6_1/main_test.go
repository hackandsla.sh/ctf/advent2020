package main

import "testing"

func Test_getYesses(t *testing.T) {
	type args struct {
		input []byte
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			args: args{
				input: []byte(`abc

a
b
c

ab
ac

a
a
a
a

b`),
			},
			want: 11,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getYesses(tt.args.input); got != tt.want {
				t.Errorf("getYesses() = %v, want %v", got, tt.want)
			}
		})
	}
}
