package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	inputs, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	var nums []int
	for _, input := range bytes.Split(inputs, []byte("\n")) {
		i, err := strconv.Atoi(string(input))
		if err != nil {
			return err
		}

		nums = append(nums, i)
	}

	fmt.Println(nums)

	for i := 0; i < len(nums); i++ {
		numI := nums[i]

		for j := 0; j < len(nums); j++ {
			numJ := nums[j]

			if numI+numJ == 2020 {
				fmt.Printf("%d * %d = %d\n", numI, numJ, numI*numJ)
			}
		}
	}

	return nil
}
