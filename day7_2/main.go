package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		log.Fatal(err)
	}

	if err := run(input); err != nil {
		log.Fatal(err)
	}
}

func run(input []byte) error {
	count, err := Count(input)
	if err != nil {
		return err
	}

	fmt.Println(count)
	return nil
}

func Count(input []byte) (int, error) {
	split := strings.Split(string(input), "\n")

	rules := map[string][]Child{}
	for _, line := range split {
		splitLine := strings.Split(line, "contain")
		bag := splitLine[0]
		bagColor := bag[0 : len(bag)-len(" bag ")-1]
		contents := splitLine[1]

		contents = strings.Trim(contents, ". ")

		var children []Child

		if strings.Contains(contents, "no other bags") {
			rules[bagColor] = children
			continue
		}

		splitContents := strings.Split(contents, ",")
		for _, child := range splitContents {
			child = strings.TrimSpace(child)
			numString := child[0]

			num, err := strconv.Atoi(string(numString))
			if err != nil {
				return 0, err
			}

			childColor := child[2:]
			childColor = strings.TrimSuffix(childColor, " bags")
			childColor = strings.TrimSuffix(childColor, " bag")

			children = append(children, Child{
				Num:   num,
				Color: childColor,
			})
		}

		rules[bagColor] = children
	}

	return countChildren("shiny gold", rules) - 1, nil
}

type Child struct {
	Num   int
	Color string
}

func countChildren(color string, rules map[string][]Child) int {
	count := 1

	children := rules[color]

	if len(children) == 0 {
		return count
	}

	for _, child := range children {
		descendentCount := countChildren(child.Color, rules)
		count += child.Num * descendentCount
	}

	return count
}
