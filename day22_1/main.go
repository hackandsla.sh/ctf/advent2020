package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	score, err := GetWinnerScore(string(input))
	if err != nil {
		return err
	}

	fmt.Println(score)
	return nil
}

func GetWinnerScore(input string) (int, error) {
	players := strings.Split(string(input), "\n\n")
	var player1 []int
	var player2 []int
	p1Split := strings.Split(players[0], "\n")
	for i := 1; i < len(p1Split); i++ {
		num, err := strconv.Atoi(p1Split[i])
		if err != nil {
			return 0, err
		}

		player1 = append(player1, num)
	}

	p2Split := strings.Split(players[1], "\n")
	for i := 1; i < len(p2Split); i++ {
		num, err := strconv.Atoi(p2Split[i])
		if err != nil {
			return 0, err
		}

		player2 = append(player2, num)
	}

	for len(player1) != 0 && len(player2) != 0 {
		var p1Card int
		var p2Card int
		p1Card, player1 = player1[0], player1[1:]
		p2Card, player2 = player2[0], player2[1:]
		if p1Card > p2Card {
			player1 = append(player1, p1Card)
			player1 = append(player1, p2Card)
		} else {
			player2 = append(player2, p2Card)
			player2 = append(player2, p1Card)
		}
	}

	winner := player1
	if len(player1) == 0 {
		winner = player2
	}

	var score int
	for i, card := range winner {
		score += card * (len(winner) - i)
	}

	return score, nil
}
