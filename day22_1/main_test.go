package main

import "testing"

func TestGetWinnerScore(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			args: args{
				input: `Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10`,
			},
			want: 306,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetWinnerScore(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetWinnerScore() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetWinnerScore() = %v, want %v", got, tt.want)
			}
		})
	}
}
