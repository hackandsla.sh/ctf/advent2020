package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"sort"
	"strconv"
	"strings"

	"github.com/kr/pretty"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	lines := strings.Split(string(input), "\n")
	adapters := make([]int, len(lines))
	for i, line := range lines {
		adapter, err := strconv.Atoi(line)
		if err != nil {
			return err
		}

		adapters[i] = adapter
	}

	sort.Ints(adapters)

	diffCount := map[int]int{
		1: 0,
		2: 0,
		3: 0,
	}
	prev := 0
	for _, adapter := range adapters {
		diffCount[adapter-prev]++

		prev = adapter
	}

	diffCount[3]++

	pretty.Println(diffCount)
	fmt.Printf("%d * %d = %d\n", diffCount[1], diffCount[3], diffCount[1]*diffCount[3])
	return nil
}
