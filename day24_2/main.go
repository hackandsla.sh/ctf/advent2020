package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"time"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	num := numFlippedTiles(string(input), 100)
	fmt.Println(num)
	return nil
}

func numFlippedTiles(input string, days int) int {
	var directions [][]HexCoord
	lines := strings.Split(input, "\n")
	for _, line := range lines {
		var path []HexCoord
		for i := 0; i < len(line); i++ {
			var coord HexCoord
			char := line[i]
			if char == 's' || char == 'n' {
				i++
				horizontalChar := line[i]
				switch char {
				case 's':
					coord.Y = -1
				case 'n':
					coord.Y = 1
				}

				switch horizontalChar {
				case 'e':
					coord.X = 1
				case 'w':
					coord.X = -1
				}
			} else {
				switch char {
				case 'e':
					coord.X = 1
				case 'w':
					coord.X = -1
				}
			}

			path = append(path, coord)
		}

		directions = append(directions, path)
	}

	floor := &Floor{}
	for _, path := range directions {
		current := HexCoord{}
		target := current.Traverse(path)

		floor.Flip(target)
	}

	i := 0

	done := make(chan bool)
	ticker := time.NewTicker(1 * time.Second)

	go func() {
		p := message.NewPrinter(language.English)
		for {
			select {
			case <-done:
				ticker.Stop()
				return
			case <-ticker.C:
				p.Printf("\rvalue: %d", i)
			}
		}
	}()

	size := 100
	for i = 0; i < days; i++ {

		var toFlip []HexCoord
		for x := -size; x < size; x++ {
			for y := -size; y < size; y++ {
				current := HexCoord{X: x, Y: y}
				numBlackNeighbors := floor.NumBlackNeighbors(current)
				if floor.IsBlack(current) && (numBlackNeighbors == 0 || numBlackNeighbors > 2) {
					toFlip = append(toFlip, current)
				}

				if !floor.IsBlack(current) && numBlackNeighbors == 2 {
					toFlip = append(toFlip, current)
				}
			}
		}

		for _, item := range toFlip {
			floor.Flip(item)
		}
	}
	return len(floor.blackTiles)
}

type HexCoord struct {
	X int
	Y int
}

func (h HexCoord) Traverse(path []HexCoord) HexCoord {
	for _, vector := range path {
		if h.Y%2 == 0 && vector.Y != 0 && vector.X == -1 {
			vector.X = 0
		}
		if h.Y%2 != 0 && vector.Y != 0 && vector.X == 1 {
			vector.X = 0
		}

		h.X += vector.X
		h.Y += vector.Y
	}

	return h
}

type Floor struct {
	blackTiles []HexCoord
}

func (f *Floor) Flip(hc HexCoord) {
	foundMatch := -1
	for i, tile := range f.blackTiles {
		if tile.X == hc.X && tile.Y == hc.Y {
			foundMatch = i
			break
		}
	}

	if foundMatch == -1 {
		f.blackTiles = append(f.blackTiles, hc)
	} else {
		f.blackTiles = append(f.blackTiles[:foundMatch], f.blackTiles[foundMatch+1:]...)
	}
}

func (f *Floor) NumBlackNeighbors(hc HexCoord) int {
	allDirections := []HexCoord{
		{X: 1, Y: -1},
		{X: 1, Y: 0},
		{X: 1, Y: 1},
		{X: -1, Y: -1},
		{X: -1, Y: 0},
		{X: -1, Y: 1},
	}

	var neighbors []HexCoord
	for _, dir := range allDirections {
		neighbors = append(neighbors, hc.Traverse([]HexCoord{dir}))
	}

	var count int
	for _, neighbor := range neighbors {
		if f.IsBlack(neighbor) {
			count++
		}
	}
	return count
}

func (f *Floor) IsBlack(hc HexCoord) bool {
	for _, tile := range f.blackTiles {
		if tile.X == hc.X && tile.Y == hc.Y {
			return true
		}
	}

	return false
}
