package main

import "testing"

func TestMatchesRule(t *testing.T) {
	testRules, err := parseRules(`0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"`)
	if err != nil {
		t.Fatal(err)
	}

	type args struct {
		s     string
		rules map[int]rule
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			args: args{
				s:     "ababbb",
				rules: testRules,
			},
			want: true,
		},
		{
			args: args{
				s:     "abbbab",
				rules: testRules,
			},
			want: true,
		},
		{
			args: args{
				s:     "bababa",
				rules: testRules,
			},
			want: false,
		},
		{
			args: args{
				s:     "aaabbb",
				rules: testRules,
			},
			want: false,
		},
		{
			args: args{
				s:     "aaaabbb",
				rules: testRules,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MatchesRule(tt.args.s, tt.args.rules); got != tt.want {
				t.Errorf("MatchesRule() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_matchesRule(t *testing.T) {
	type args struct {
		s     string
		r     rule
		rules map[int]rule
		index int
	}
	tests := []struct {
		name        string
		args        args
		wantMatches bool
		wantLength  int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotMatches, gotLength := matchesRule(tt.args.s, tt.args.r, tt.args.rules, tt.args.index)
			if gotMatches != tt.wantMatches {
				t.Errorf("matchesRule() gotMatches = %v, want %v", gotMatches, tt.wantMatches)
			}
			if gotLength != tt.wantLength {
				t.Errorf("matchesRule() gotLength = %v, want %v", gotLength, tt.wantLength)
			}
		})
	}
}
