package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"

	"github.com/kr/pretty"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	lines := strings.Split(string(input), "\n")

	var instructions []instruction
	for _, line := range lines {
		split := strings.Split(line, " ")
		arg, err := strconv.Atoi(split[1])
		if err != nil {
			return err
		}

		instructions = append(instructions, instruction{
			op:  split[0],
			arg: arg,
		})
	}

	pretty.Println(instructions)

outer:
	for i := 0; i < len(instructions); i++ {
		if instructions[i].op == "acc" {
			continue
		}

		if instructions[i].op == "jmp" {
			cp := make([]instruction, len(instructions))
			copy(cp, instructions)
			cp[i].op = "nop"

			e := executor{
				accumulator:  0,
				current:      0,
				executed:     make(map[int]struct{}),
				instructions: cp,
			}

			for {
				e.next()
				if _, ok := e.executed[e.current]; ok {
					continue outer
				}

				if e.completed {
					fmt.Println(e.accumulator)
					return nil
				}
			}
		}

		if instructions[i].op == "nop" {
			cp := make([]instruction, len(instructions))
			copy(cp, instructions)

			cp[i].op = "jmp"

			e := executor{
				accumulator:  0,
				current:      0,
				executed:     make(map[int]struct{}),
				instructions: cp,
			}

			for {
				e.next()
				if _, ok := e.executed[e.current]; ok {
					continue outer
				}

				if e.completed {
					fmt.Println(e.accumulator)
					return nil
				}
			}
		}
	}

	return errors.New("couldn't find flipped instruction")
}

type executor struct {
	accumulator  int
	current      int
	executed     map[int]struct{}
	instructions []instruction
	completed    bool
}

func (e *executor) next() {
	if e.current >= len(e.instructions) {
		// noop to prevent trying to access invalid instructions
		e.completed = true
		return
	}

	instr := e.instructions[e.current]
	e.executed[e.current] = struct{}{}

	switch instr.op {
	case "acc":
		e.accumulator += instr.arg
		e.current++
	case "jmp":
		e.current += instr.arg
	case "nop":
		e.current++
	default:
		panic("invalid instruction '" + instr.op + "'")
	}
}

type instruction struct {
	op  string
	arg int
}
