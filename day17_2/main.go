package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	size := 51
	space := init4DSpace(size)

	lines := strings.Split(string(input), "\n")
	xStart := size/2 - (len(lines[0]) / 2)
	yStart := size/2 - (len(lines) / 2)
	zStart := size / 2
	wStart := size / 2
	for y, line := range lines {
		for x, char := range line {
			if char == '#' {
				space[xStart+x][yStart+y][zStart][wStart] = true
			}
		}
	}

	for i := 0; i < 6; i++ {
		space = step(space)
	}

	fmt.Println(count(space))
	return nil
}

func init4DSpace(size int) [][][][]bool {
	space := make([][][][]bool, size)
	for x := 0; x < len(space); x++ {
		space[x] = make([][][]bool, size)
		for y := 0; y < len(space[x]); y++ {
			space[x][y] = make([][]bool, size)
			for z := 0; z < len(space[x][y]); z++ {
				space[x][y][z] = make([]bool, size)
			}
		}
	}

	return space
}

func count(space [][][][]bool) int {
	count := 0
	for x := 0; x < len(space); x++ {
		for y := 0; y < len(space[x]); y++ {
			for z := 0; z < len(space[x][y]); z++ {
				for w := 0; w < len(space[x][y][z]); w++ {
					if space[x][y][z][w] {
						count++
					}
				}
			}
		}
	}

	return count
}

func step(space [][][][]bool) [][][][]bool {
	next := init4DSpace(len(space))

	for x := 0; x < len(space); x++ {
		for y := 0; y < len(space[x]); y++ {
			for z := 0; z < len(space[x][y]); z++ {
				for w := 0; w < len(space[x][y][z]); w++ {
					numNeighbors := numActiveNeighbors(x, y, z, w, space)
					if space[x][y][z][w] && numNeighbors >= 2 && numNeighbors <= 3 {
						next[x][y][z][w] = true
					}

					if !space[x][y][z][w] && numNeighbors == 3 {
						next[x][y][z][w] = true
					}
				}

			}
		}
	}

	return next
}

func numActiveNeighbors(x, y, z, w int, space [][][][]bool) int {
	count := 0
	for xIdx := x - 1; xIdx <= x+1; xIdx++ {
		if xIdx < 0 || xIdx >= len(space) {
			continue
		}

		for yIdx := y - 1; yIdx <= y+1; yIdx++ {
			if yIdx < 0 || yIdx >= len(space[x]) {
				continue
			}

			for zIdx := z - 1; zIdx <= z+1; zIdx++ {
				if zIdx < 0 || zIdx >= len(space[x][y]) {
					continue
				}

				for wIdx := w - 1; wIdx <= w+1; wIdx++ {
					if wIdx < 0 || wIdx >= len(space[x][y][z]) {
						continue
					}

					if xIdx == x && yIdx == y && zIdx == z && wIdx == w {
						// Skip the cell being evaluated
						continue
					}

					if space[xIdx][yIdx][zIdx][wIdx] {
						count++
					}
				}
			}
		}
	}

	return count
}
