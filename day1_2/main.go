package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	inputs, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	var nums []int
	for _, input := range bytes.Split(inputs, []byte("\n")) {
		i, err := strconv.Atoi(string(input))
		if err != nil {
			return err
		}

		nums = append(nums, i)
	}

	for i := 0; i < len(nums); i++ {
		for j := 0; j < len(nums); j++ {
			for k := 0; k < len(nums); k++ {
				if nums[i]+nums[j]+nums[k] == 2020 {
					fmt.Printf("%d * %d * %d = %d\n", nums[i], nums[j], nums[k], nums[i]*nums[j]*nums[k])
				}
			}
		}
	}

	return nil
}
