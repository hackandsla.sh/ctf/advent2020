package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	seats := bytes.Split(input, []byte{'\n'})

	for {
		next := step(seats)
		if compare2DBytes(next, seats) {
			break
		}

		seats = next
	}

	occupied := 0
	for _, row := range seats {
		for _, seat := range row {
			if seat == '#' {
				occupied++
			}
		}
	}

	fmt.Println(occupied)
	return nil
}

type coordinate struct {
	X int
	Y int
}

func print2DBytes(rows [][]byte) {
	for _, row := range rows {
		fmt.Println(string(row))
	}
}

func compare2DBytes(b1, b2 [][]byte) bool {
	for r := range b1 {
		for c := range b1 {
			if b1[r][c] != b2[r][c] {
				return false
			}
		}
	}

	return true
}

func step(seats [][]byte) [][]byte {
	// Create a deep copy of our state
	next := make([][]byte, len(seats))
	for i, row := range seats {
		next[i] = make([]byte, len(row))
		copy(next[i], row)
	}

	for r, row := range seats {
		for c, seat := range row {
			adjacents := []coordinate{
				{X: c - 1, Y: r - 1},
				{X: c, Y: r - 1},
				{X: c + 1, Y: r - 1},
				{X: c - 1, Y: r},
				{X: c + 1, Y: r},
				{X: c - 1, Y: r + 1},
				{X: c, Y: r + 1},
				{X: c + 1, Y: r + 1},
			}
			occupiedAdjacents := 0
			for _, adjacent := range adjacents {
				if adjacent.X < 0 || adjacent.Y < 0 {
					continue
				}

				if adjacent.X >= len(row) || adjacent.Y >= len(seats) {
					continue
				}

				if seats[adjacent.Y][adjacent.X] == '#' {
					occupiedAdjacents++
				}
			}

			if seat == 'L' && occupiedAdjacents == 0 {
				next[r][c] = '#'
			}

			if seat == '#' && occupiedAdjacents >= 4 {
				next[r][c] = 'L'
			}
		}
	}

	return next
}
