package main

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	score, err := GetWinnerScore(string(input))
	if err != nil {
		return err
	}

	fmt.Println(score)
	return nil
}

func GetWinnerScore(input string) (int, error) {
	players := strings.Split(string(input), "\n\n")
	var player1 []int
	var player2 []int
	p1Split := strings.Split(players[0], "\n")
	for i := 1; i < len(p1Split); i++ {
		num, err := strconv.Atoi(p1Split[i])
		if err != nil {
			return 0, err
		}

		player1 = append(player1, num)
	}

	p2Split := strings.Split(players[1], "\n")
	for i := 1; i < len(p2Split); i++ {
		num, err := strconv.Atoi(p2Split[i])
		if err != nil {
			return 0, err
		}

		player2 = append(player2, num)
	}

	_, score := Game(player1, player2)

	return score, nil
}

func Game(player1, player2 []int) (winner int, score int) {
	dc := &DuplicateChecker{}
	for len(player1) != 0 && len(player2) != 0 {
		if dc.Check(player1, player2) {
			return 0, GetScore(player1)
		}
		var p1Card int
		var p2Card int
		p1Card, player1 = player1[0], player1[1:]
		p2Card, player2 = player2[0], player2[1:]

		var winner int
		if p1Card <= len(player1) && p2Card <= len(player2) {
			p1Copy := make([]int, p1Card)
			copy(p1Copy, player1)

			p2Copy := make([]int, p2Card)
			copy(p2Copy, player2)

			winner, _ = Game(p1Copy, p2Copy)
		} else if p1Card > p2Card {
			winner = 0
		} else {
			winner = 1
		}

		if winner == 0 {
			player1 = append(player1, p1Card)
			player1 = append(player1, p2Card)
		} else {
			player2 = append(player2, p2Card)
			player2 = append(player2, p1Card)
		}
	}

	if len(player1) == 0 {
		return 1, GetScore(player2)
	}

	return 0, GetScore(player1)
}

func GetScore(cards []int) int {
	var score int
	for i, card := range cards {
		score += card * (len(cards) - i)
	}
	return score
}

type DuplicateChecker struct {
	hashes [][]byte
}

func (dc *DuplicateChecker) Check(p1, p2 []int) bool {
	var p1Bytes []byte
	for _, card := range p1 {
		p1Bytes = append(p1Bytes, byte(card))
	}

	var p2Bytes []byte
	for _, card := range p2 {
		p2Bytes = append(p2Bytes, byte(card))
	}

	p1Hash := sha256.Sum256(p1Bytes)
	p2Hash := sha256.Sum256(p2Bytes)

	combinedHash := sha256.Sum256(append(p1Hash[:], p2Hash[:]...))

	if dc.checkHashes(combinedHash[:]) {
		return true
	}

	dc.hashes = append(dc.hashes, combinedHash[:])
	return false
}

func (dc *DuplicateChecker) checkHashes(hash []byte) bool {
	for _, h := range dc.hashes {
		if bytes.Equal(hash, h) {
			return true
		}
	}
	return false
}
