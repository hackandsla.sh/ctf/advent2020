package main

import (
	"math/big"
	"reflect"
	"testing"
)

func Test_getNumCombos(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		want    *big.Int
		wantErr bool
	}{
		{
			args: args{
				input: `16
10
15
5
1
11
7
19
6
12
4`,
			},
			want: big.NewInt(8),
		},
		{
			args: args{
				input: `28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3`,
			},
			want: big.NewInt(19208),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getNumCombos(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("getNumCombos() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getNumCombos() = %v, want %v", got, tt.want)
			}
		})
	}
}
