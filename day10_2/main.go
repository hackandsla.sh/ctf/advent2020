package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"sort"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	count, err := getNumCombos(string(input))
	if err != nil {
		return err
	}

	fmt.Println(count.String())
	return nil
}

func getNumCombos(input string) (*big.Int, error) {
	lines := strings.Split(string(input), "\n")
	adapters := make([]int, len(lines))
	for i, line := range lines {
		adapter, err := strconv.Atoi(line)
		if err != nil {
			return nil, err
		}

		adapters[i] = adapter
	}

	sort.Ints(adapters)

	adapters = append(adapters, adapters[len(adapters)-1]+3)

	comboCount := big.NewInt(1)
	oneChain := 0
	prev := 0
	for _, adapter := range adapters {
		if adapter-prev == 1 {
			oneChain++
		} else {
			if oneChain > 0 {
				factors := map[int]int64{
					1: 1,
					2: 2,
					3: 4,
					4: 7,
				}
				comboCount = comboCount.Mul(comboCount, big.NewInt(factors[oneChain]))
			}

			oneChain = 0
		}

		prev = adapter
	}

	return comboCount, nil
}
