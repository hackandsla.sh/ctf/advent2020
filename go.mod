module gitlab.com/hackandsla.sh/ctf/advent2020

go 1.15

require (
	github.com/go-ozzo/ozzo-validation/v4 v4.3.0
	github.com/kr/pretty v0.2.1
	github.com/mitchellh/copystructure v1.0.0
	golang.org/x/text v0.3.4
	gopkg.in/yaml.v2 v2.2.2
)
