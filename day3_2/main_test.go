package main

import (
	"strings"
	"testing"
)

func Test_getHitCount(t *testing.T) {
	type args struct {
		right int
		down  int
		lines [][]byte
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			args: args{
				right: 1,
				down:  1,
				lines: getTestInput(),
			},
			want: 2,
		},
		{
			args: args{
				right: 3,
				down:  1,
				lines: getTestInput(),
			},
			want: 7,
		},
		{
			args: args{
				right: 5,
				down:  1,
				lines: getTestInput(),
			},
			want: 3,
		},
		{
			args: args{
				right: 7,
				down:  1,
				lines: getTestInput(),
			},
			want: 4,
		},
		{
			args: args{
				right: 1,
				down:  2,
				lines: getTestInput(),
			},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getHitCount(tt.args.right, tt.args.down, tt.args.lines); got != tt.want {
				t.Errorf("getHitCount() = %v, want %v", got, tt.want)
			}
		})
	}
}

const testInput = `..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#`

func getTestInput() [][]byte {
	var ret [][]byte

	lines := strings.Split(testInput, "\n")
	for _, line := range lines {
		ret = append(ret, []byte(line))
	}

	return ret
}
