package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	lines := bytes.Split(input, []byte("\n"))

	result := getHitCount(1, 1, lines) *
		getHitCount(3, 1, lines) *
		getHitCount(5, 1, lines) *
		getHitCount(7, 1, lines) *
		getHitCount(1, 2, lines)

	// result := getHitCount(3, 1, lines)
	fmt.Println(result)
	return nil
}

func getHitCount(right, down int, lines [][]byte) int {
	hitCount := 0
	idx := 0
	for i := 0; i < len(lines); i += down {
		line := lines[i]
		char := line[idx%len(line)]
		if char == []byte("#")[0] {
			hitCount++
		}
		idx += right
	}

	return hitCount
}
