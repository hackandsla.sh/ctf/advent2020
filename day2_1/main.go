package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	inputs, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	passwords := strings.Split(string(inputs), "\n")

	var policies []policy
	for _, password := range passwords {
		min, err := strconv.Atoi(password[0:strings.Index(password, "-")])
		if err != nil {
			return err
		}

		max, err := strconv.Atoi(password[strings.Index(password, "-")+1 : strings.Index(password, " ")])
		if err != nil {
			return err
		}

		char := password[strings.Index(password, " ")+1 : strings.Index(password, " ")+2]

		pass := password[strings.Index(password, ": ")+2:]

		policies = append(policies, policy{
			Min:      min,
			Max:      max,
			Char:     char,
			Password: pass,
		})
	}

	fmt.Println(policies)

	count := 0
	for _, policy := range policies {
		numChars := strings.Count(policy.Password, policy.Char)
		if numChars <= policy.Max && numChars >= policy.Min {
			count++
		}
	}

	fmt.Println(count)
	return nil
}

type policy struct {
	Password string
	Min      int
	Max      int
	Char     string
}
