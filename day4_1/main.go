package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	count, err := getValidPassports(string(input))
	if err != nil {
		return err
	}

	fmt.Println(count)
	return nil
}

func getValidPassports(input string) (int, error) {
	split := strings.Split(string(input), "\n\n")
	// fmt.Println(strings.Join(split, " | "))

	fmt.Println(len(split))
	var ps []passport
	for _, s := range split {
		p := passport{}
		lines := strings.Split(s, "\n")
		for _, line := range lines {
			kvs := strings.Split(line, " ")
			for _, kv := range kvs {
				if kv == "" {
					break
				}
				kvSplit := strings.Split(kv, ":")
				// fmt.Println(kvSplit)
				key, value := kvSplit[0], kvSplit[1]
				switch key {
				case "byr":
					p.byr = value
				case "iyr":
					p.iyr = value
				case "eyr":
					p.eyr = value
				case "hgt":
					p.hgt = value
				case "hcl":
					p.hcl = value
				case "ecl":
					p.ecl = value
				case "pid":
					p.pid = value
				case "cid":
					p.cid = value
				default:
					fmt.Printf("unknown key: %s\n", key)
				}
			}
		}

		ps = append(ps, p)
	}

	count := 0
	for _, p := range ps {
		if err := p.Validate(); err == nil {
			count++
		}
	}

	return count, nil
}

type passport struct {
	byr string
	iyr string
	eyr string
	hgt string
	hcl string
	ecl string
	pid string
	cid string
}

func (p *passport) Validate() error {
	return validation.ValidateStruct(p,
		validation.Field(&p.byr, validation.Required),
		validation.Field(&p.iyr, validation.Required),
		validation.Field(&p.hgt, validation.Required),
		validation.Field(&p.eyr, validation.Required),
		validation.Field(&p.hcl, validation.Required),
		validation.Field(&p.ecl, validation.Required),
		validation.Field(&p.pid, validation.Required),
	)
}
