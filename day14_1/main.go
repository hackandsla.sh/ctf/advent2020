package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	sum, err := getMemSum(string(input))
	if err != nil {
		return err
	}

	fmt.Println(sum)

	return nil
}

func getMemSum(input string) (int, error) {
	lines := strings.Split(input, "\n")

	mask := map[int]int{}
	mem := map[int]int{}

	for _, line := range lines {
		if strings.HasPrefix(line, "mask = ") {
			mask = map[int]int{}
			maskString := strings.TrimPrefix(line, "mask = ")
			for idx, char := range maskString {
				if char == 'X' {
					continue
				}

				num, err := strconv.Atoi(string(char))
				if err != nil {
					return 0, err
				}

				mask[35-idx] = num
			}

			continue
		}

		re := regexp.MustCompile(`mem\[(\d+)\] = (\d+)`)
		matches := re.FindStringSubmatch(line)
		key, err := strconv.Atoi(matches[1])
		if err != nil {
			return 0, err
		}

		value, err := strconv.Atoi(matches[2])
		if err != nil {
			return 0, err
		}

		mem[key] = getMaskedInt(value, mask)
	}

	sum := 0
	for _, v := range mem {
		sum += v
	}

	return sum, nil
}

func getMaskedInt(i int, mask map[int]int) int {
	for k, v := range mask {
		m := 1 << k
		if v == 1 {
			i = i | m
		} else if v == 0 {
			i = i &^ m
		}
	}

	return i
}
