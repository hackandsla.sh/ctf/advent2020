package main

import (
	"testing"
)

func Test_getMaskedInt(t *testing.T) {
	type args struct {
		i    int
		mask map[int]int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			args: args{
				i: 11,
				mask: map[int]int{
					1: 0,
					6: 1,
				},
			},
			want: 73,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getMaskedInt(tt.args.i, tt.args.mask); got != tt.want {
				t.Errorf("getMaskedInt() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getMemSum(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			args: args{
				input: `mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0`,
			},
			want: 165,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getMemSum(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("getMemSum() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getMemSum() = %v, want %v", got, tt.want)
			}
		})
	}
}
