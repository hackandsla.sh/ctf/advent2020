package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
)

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	input, err := ioutil.ReadFile("input.txt")
	if err != nil {
		return err
	}

	lines := strings.Split(string(input), "\n")
	sum := 0
	for _, line := range lines {
		sum += calculate(line)
	}

	fmt.Println(sum)
	return nil
}

func calculate(s string) int {
	result := 0
	lastOp := "+"

	for i := 0; i < len(s); i++ {
		char := s[i]

		switch char {
		case ' ':
			continue
		case '+':
			lastOp = "+"
		case '*':
			lastOp = "*"

		case '(':
			// find the matching close paren
			matchingCloseIndex := -1

			parenStack := 1
			for j := i + 1; j < len(s); j++ {
				if s[j] == '(' {
					parenStack++
				}
				if s[j] == ')' {
					parenStack--
				}

				if parenStack == 0 {
					matchingCloseIndex = j
					break
				}
			}

			subexpr := calculate(s[i+1 : matchingCloseIndex])
			switch lastOp {
			case "+":
				result += subexpr
			case "*":
				result *= subexpr
			}
			i = matchingCloseIndex
		case ')':
			continue
		default:
			operand, err := strconv.Atoi(string(char))
			if err != nil {
				panic(err)
			}

			switch lastOp {
			case "+":
				result += operand
			case "*":
				result *= operand
			}
		}

	}

	return result
}
