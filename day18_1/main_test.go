package main

import "testing"

func Test_calculate(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			args: args{
				s: "2 * 3 + (4 * 5)",
			},
			want: 26,
		},
		{
			args: args{
				s: "5 + (8 * 3 + 9 + 3 * 4 * 3)",
			},
			want: 437,
		},
		{
			args: args{
				s: "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))",
			},
			want: 12240,
		},
		{
			args: args{
				s: "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2",
			},
			want: 13632,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculate(tt.args.s); got != tt.want {
				t.Errorf("calculate() = %v, want %v", got, tt.want)
			}
		})
	}
}
